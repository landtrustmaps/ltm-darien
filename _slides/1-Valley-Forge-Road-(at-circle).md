---
title: Valley Forge
geojson: >-
  {"type":"Feature","properties":{},"geometry":{"type":"MultiPolygon","coordinates":[[[[-73.48974471127724,41.107288194535954],[-73.48956916471244,41.10723309619998],[-73.48951262021953,41.107244177304345],[-73.48948247072308,41.10708364368826],[-73.4894660150185,41.107008964293556],[-73.48939166264306,41.106671536621334],[-73.4906975238269,41.10667485191556],[-73.49085231972441,41.10667766322553],[-73.49086933625765,41.107359321172346],[-73.48974471127724,41.107288194535954]]],[[[-73.48816743723401,41.105754867156044],[-73.48921424771059,41.10585593520781],[-73.48927135005883,41.105874064566244],[-73.48930334968155,41.105902484066824],[-73.4893175476637,41.10593864886016],[-73.4892449735498,41.10616313590611],[-73.48920393241674,41.1063619760703],[-73.48919067426324,41.106562914502355],[-73.48919945194342,41.1066845000843],[-73.48824534615356,41.10669629612655],[-73.48816743723401,41.105754867156044]]]]}}
ltmid: '1'
size: 4
image: /dist/assets/valley-forge.jpg
categories:
  - category: children
  - category: birding
---
The two open spaces at Valley Forge, totaling more than four acres, were donated to the DLT by the Paxton Mendelssohn Estate in 2006.

Since that time we have replaced the old dam, removed a garage, and restored the habitat by planting native plants, removing invasives, and rebuilding walls.

Many birds and animals now make this peaceful pond their home and quiet observation from the bridge on Half Mile Road will reveal wood ducks and heron.
