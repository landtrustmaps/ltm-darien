---
title: Dunlap Woods
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.463536,41.078862],[-73.461174,41.079136],[-73.461175,41.079151],[-73.460684,41.080022],[-73.460179,41.080141],[-73.458809,41.080463],[-73.458563,41.077714],[-73.460327,41.077743],[-73.461061,41.077755],[-73.463691,41.077803],[-73.464224,41.077813],[-73.463536,41.078862]]]}}]}
ltmid: '2'
size: 22
image: /dist/assets/01_dunlap_woods.jpg
categories:
  - category: children
  - category: fishing
  - category: birding
  - category: hiking
---
Dunlap Woods is a 22-acre Nature Preserve that is adjacent to the Town of Darien’s 28-acre Selleck’s Woods.

This was the first property acquired by the Darien Land Trust and together these two parcels consist of seven interlinked ecosystems with trails and natural features.

Many community groups. including Darien schools, use these trails for nature walks and educational visits for youngsters.

This Nature Preserve is open to the public from dawn to dusk and we encourage visitors to walk these wide and well-maintained trails.

Dogs are welcome, but must be on leashes.
