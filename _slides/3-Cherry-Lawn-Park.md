---
title: Cherry Lawn
geojson: >-
  {"type":"Feature","properties":{},"geometry":{"type":"MultiPolygon","coordinates":[[[[-73.46521201996981,41.089128379696504],[-73.46512119738509,41.08914338792781],[-73.4645599910479,41.089236125782165],[-73.46435372788106,41.08927015955561],[-73.46351364566625,41.08940882752935],[-73.46315799512519,41.08946748123548],[-73.46303434130274,41.08948787466816],[-73.46300345831875,41.0893405159724],[-73.46294251205909,41.08906437460789],[-73.46289057689964,41.08882942880704],[-73.46381399951275,41.088653249610275],[-73.46394519970967,41.088621467321325],[-73.46457409203946,41.088485056303035],[-73.46476890807972,41.08844279947109],[-73.46489637557933,41.08840879158888],[-73.46521201996981,41.089128379696504]]]]}}
ltmid: '3'
image: /dist/assets/00_cherrylawn.jpg
categories:
  - category: children
  - category: hiking
  - category: birding
---
Directly behind the [Darien Nature Center](https://dariennaturecenter.org/) is another of the DLT’s open access properties.

This three-acre parcel has trails to walk on through a dense woodland area that adjoins Cherry Lawn park.

This property is open from dawn to dusk and dogs on a leash are welcome.
