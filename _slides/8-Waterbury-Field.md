---
title: Waterbury Field
geojson: >-
  {"type":"Feature","properties":{},"geometry":{"type":"MultiPolygon","coordinates":[[[[-73.49848188778263,41.08755906319747],[-73.4977464201289,41.08767009851492],[-73.49771499983787,41.08766967295775],[-73.4976425624134,41.087648052801434],[-73.49758991067092,41.087592845889496],[-73.49726804915588,41.086915351256444],[-73.49827882855199,41.08677762155397],[-73.49848188778263,41.08755906319747]]]]}}
ltmid: '8'
image: /dist/assets/waterbury.jpg
categories:
  - category: birding
  - category: children
---
Waterbury Field was purchased from the Joslin family in 2008 with generous donations from many residents of the Town.

The Darien Land Trust will maintain this property as an example of a bio-diverse suburban habitat using native plants.

We invite passers-by to observe the activity at the two bluebird boxes that are placed in the meadow.
